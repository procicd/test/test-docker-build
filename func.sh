
# For local testing of build args function

makebuildargs() {
    buildargs=""
    for arg in $(echo $1); do
        buildargs="$buildargs --build-arg '$arg=\$$arg'"
    done
    buildargs="${buildargs% }"
    eval "result=\"$buildargs\""
    echo "$result"
}
ARG1='foo'
ARG2='bar1 bar2'
RESOLVED_BUILD_ARGS=$(makebuildargs 'ARG1 ARG2')
echo "$RESOLVED_BUILD_ARGS"
eval docker build --no-cache -t docker-sandbox . $RESOLVED_BUILD_ARGS
docker run docker-sandbox
